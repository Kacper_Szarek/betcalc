# README #

### Important info ###

* This is a Wordpress plugin, it should be used on Wordpress site.
* It was tested on WP 5.5.3
* It require ACF Pro plugin (it's included in this repo)
* For RapidAPI I created dev account based on my personal details. There is a limit of 500 request per month in free version.

* There is no filter option as I created this plugin in my free time. Filter version could be done additionally.
* There is odds showed for first match in API response to keep it clear on page. It's just example how to get values from API.
* As for now there is only Decimal (European) version for calculate the payout.

### How to show it on site ###

* Install Wordpress (version 5.5.3 - latest)
* After you download .zip or clone the repo you need to place the folders (betcalc and advanced-custom-fields-pro) in /wp-content/plugins
* Activate both ACF Pro and BetCalc
* Go to any page and add new Gutenberg block named 'BetCalc'
* Fill the fields in the block edit view
* Save the page
* Go to page and enjoy you BetCalc block ;)