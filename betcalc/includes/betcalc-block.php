<?php

/*
*
* Registration and configuration of a 'BetCalc' Gutenberg block
*
*/

class BetCalc_Block {

	/*
	*
	* betcalc_load_block
	* 
	* Load JS script from plugin dir
	*
	*/

	public static function betcalc_gutenberg_block() {
		if( function_exists('acf_register_block') ) {
			acf_register_block(array(
	            'name'  => 'betcalc',
	            'title' => __('BetCalc'),
	            'description'   => __('Betcalc Gutenber Block'),
	            'render_template'   => BETCALC_PLUGIN_DIR . 'includes/betcalc-content.php',
	            'category'  => 'formatting',
	            'icon'  => 'admin-settings',
	            'keywords'  => array( 'betcalc' ),
	            'enqueue_style' => plugin_dir_url( __DIR__ ) . 'assets/css/betcalc_block.css'
        	));
		}
	}

}

function betcalc_enqueue_script() {   
    wp_enqueue_script('betcalc-block', plugin_dir_url( __DIR__ ) . 'assets/js/betcalc_block.js', array('jquery'), null, true);
}
add_action('wp_enqueue_scripts', 'betcalc_enqueue_script');

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5fc94a6e4c594',
	'title' => 'BetCalc - Gutenberg Block',
	'fields' => array(
		array(
			'key' => 'field_5fc94ab584a49',
			'label' => 'Block title',
			'name' => 'block_title',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => 'Enter the Stake and Odds for your bet and the Bet Calculator will automatically calculate the Payout. Add Odds for Multiples.',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5fc9515f6a20b',
			'label' => 'Select span',
			'name' => 'select_span',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => 'Selected Odds Format',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/betcalc',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

endif;