<div class="betcalc-wrapper betcalc-calc">
	<h4 class="betcalc-calc-heading"><?php the_field( 'block_title' ); ?></h4>
	<hr>
	<div class="betcalc-select-wrapper">
		<span><?php the_field( 'select_span' ); ?></span>
		<select class="betcalc-select">
			<option value="decimal" selected>Decimal</option>
		</select>
	</div>
	<div class="betcalc-center-wrapper betcalc-odds-wrapper">
		<div class="betcalc-odds-item betcalc-odds-item--left">
			<span class="betcalc-stake-span">Stake</span>
			<input class="betcalc-stake" type="number" placeholder="Enter stake" value="" />
		</div>
		<div class="betcalc-odds-item betcalc-odds-item--left">
			<span class="betcalc-odds-span">Odds</span>
			<input class="betcalc-odds" type="number" placeholder="Enter odds" value="" />
		</div>
	</div>
	<div class="betcalc-center-wrapper betcalc-button-wrapper">
		<div class="betcalc-odds-item betcalc-odds-item--right">
			<button class="betcalc-addodds"><div class="betcalc-plusicon"></div>Add odds</button>
		</div>
	</div>
	<div class="betcalc-center-wrapper betcalc-clear-wrapper">
		<div id="betcalc-mdiv">
		  <div class="betcalc-mdiv">
		    <div class="betcalc-md"></div>
		  </div>
		</div>
		<span>clear</span>
	</div>
	<div class="betcalc-center-wrapper betcalc-payout-wrapper">
		<div class="betcalc-payout-item">
			Payout
		</div>
		<div class="betcalc-payout-item betcalc-payout-item--price">
			$0.00
		</div>
	</div>

	<?php

	$response = wp_remote_get( 'https://odds.p.rapidapi.com/v1/odds?region=uk&sport=soccer_epl&oddsFormat=decimal&mkt=h2h&dateFormat=iso',
	    array(
	    	'headers' => array(
	            'x-rapidapi-host' => 'odds.p.rapidapi.com',
	        	'x-rapidapi-key' => '7ae9232771mshf98eb7925b0c052p108a32jsn2935e31fe152',
	        ),
	        'httpversion' => '1.1',
	        'timeout'     => 30,
	    )
	);

	$api_response = json_decode( wp_remote_retrieve_body( $response ), true );

	$odds = $api_response['data'][0];
		
	$teams = $odds['teams'];
	$first_team = $teams[0];
	$second_team = $teams[1];
		
	$sites = $odds['sites'];
		
	?>

	<table class="betcalc-table">
		<thead>
		     <tr>
		     	<th colspan="4">
					<?php echo $first_team . ' - ' . $second_team; ?>
				</th>
			</tr>
		</thead>

		<?php 
		foreach( $sites as $site ) {
			$site_nice = $site['site_nice'];
			$odds = $site['odds'];
			$h2h = $odds['h2h'];

			$team_first_odds = $h2h[0];
			$draw_odds = $h2h[1];
			$team_second_odds = $h2h[0]; ?>

			<tr>
				<td class="page"><?php echo $site_nice; ?></td>
				<td>1 <br/> <?php echo $team_first_odds; ?></td>
				<td>X <br/> <?php echo $draw_odds; ?></td>
				<td>2 <br/> <?php echo $team_second_odds; ?></td>
			</tr>

		<?php }; ?>

	</table>
	<span class="betcalc-source">Source: https://the-odds-api.com</span>
</div>
