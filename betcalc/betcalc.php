<?php
/**
 * @package BetCalc
 */
/*
Plugin Name: BetCalc
Description: Blexr - 2nd Stage Interview | Bet Calculator which allows bettors to calculate the potential Payout for
any single bet and has a simple ‘Add Odds’ function to calculate the Payout for a multiple bet.
Version: 1.0.0
Author: Kacper Szarek
Author URI: https://www.linkedin.com/in/kacper-szarek/
License: GPLv2 or later
Text Domain: betcalc
*/

// Plugin dir
define( 'BETCALC_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );


register_activation_hook( __FILE__, 'betcalc_acf' );

function betcalc_acf(){

    // Require parent plugin
    if ( ! is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) and current_user_can( 'activate_plugins' ) ) {
        // Stop activation redirect and show error
        wp_die('Sorry, but this plugin requires the <a href="https://wordpress.org/plugins/advanced-custom-fields" target="_blank">Advanced Custom Fields</a> to be installed and active. <br><a href="' . admin_url( 'plugins.php' ) . '">&laquo; Return to Plugins</a>');
    }
}


require_once ( BETCALC_PLUGIN_DIR . 'includes/betcalc-block.php');
add_action( 'acf/init', array( 'BetCalc_Block', 'betcalc_gutenberg_block' ) );